<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2015 SAMF Dev Team.</strong> All rights reserved.
      </footer>

    </div><!-- ./wrapper -->
</body>
</html>