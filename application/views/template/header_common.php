<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SAMF - <?php echo $setTitle; ?></title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/m-logo.ico" type="image/x-icon" />
	<!-- CSS -->
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/datepicker.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/template.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/spinner.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/iCheck/all.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/css/_all-skins.min.css" rel="stylesheet" type="text/css" />
	<!-- JS -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/daterangepicker.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/raphael-min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/amcharts/amcharts.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/amcharts/serial.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.knob.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/icheck.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/app.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/amcharts/pie.js" type="text/javascript"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
          <![endif]-->
      </head>
      <body class="skin-blue">